import React from 'react';
import './App.css';
import Home from './Home';
import About from './About';
import {Route, Link} from 'react-router-dom';
import NavBar from './NavBar';
import Images from './components/Images';

function App()
{
    return (
      <div className="App">
        <div>
          <Route exact path ="/" component={Home} />
          <Route exact path ="/Gallery" component={Images} />
          <Route exact path ="/About" component={About} />
        </div>
        <footer><NavBar /></footer>  
    </div>
    
    );
}
export default App;
