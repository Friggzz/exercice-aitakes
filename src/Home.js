import React from 'react';
import Slider from './components/Slider';

function Home()
{
    return(
        <div>
            <div className="carousel-container">
                <Slider />
            </div>
            <div>
                <h1>La Minute Bidoche</h1>
            </div>
            <p>Venez ! Sautez à pieds joints avec nous dans l'abyssale fosse de l'humour (presque) hilarant ! :)</p>
        </div>
    )
}

export default Home;