import React from 'react';
import {Link} from 'react-router-dom';

function NavBar()
{
    return(
        <div className="navBar">
            <div className="navContainer">
                <div className="btn-nav"><Link to="/">Home</Link></div>
                <div className="btn-nav"><Link to="/Gallery">Liste</Link></div>
            </div>
        </div>
    );
}

export default NavBar;