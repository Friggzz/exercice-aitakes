import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import './Slider.css';

function Slider()
{
    const images = [
        {
            id: 1,
            image: 'metre.jpg',
        },
        {
            id:2,
            image: 'nezmoins.jpg',
        },
        {
            id: 3,
            image: 'lancernain.jpg',
        }
    ]

    return(
        <Carousel autoPlay interval={6000} infiniteLoop>
            {images.map(slide =>(
                <div className="pic-container" key={slide.id}>
                    <img className="pic" src={slide.image} alt="" />
                </div>
                ))
            }           
        </Carousel>
    )
}

export default Slider;